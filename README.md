# MINIM

#### Web app displaying most used words across recently published articles

##### Currently works for:
* VICE.com

##### Instructions
* 'python3 Minim.py' to run flask app server
* Visit the route '/refresh/0' to initiate & populate local database
